﻿using UnityEngine;
using DG.Tweening;

public class MissionUI : MonoBehaviour
{
    public RectTransform missionPlace;
    public MissionEntry missionEntryPrefab;
    public AdsForMission addMissionButtonPrefab;

    public void Open() {
        var chil = transform.GetChild(0);
        chil.localScale = Vector3.zero;
        gameObject.SetActive(true);
        chil.DOScale(1f, 0.2f).SetEase(Ease.Linear);

        foreach (Transform t in missionPlace)
            Destroy(t.gameObject);

        for (int i = 0; i < 3; ++i) {
            if (PlayerData.instance.missions.Count > i) {
                MissionEntry entry = Instantiate(missionEntryPrefab);
                entry.transform.SetParent(missionPlace, false);

                entry.FillWithMission(PlayerData.instance.missions[i], this);
            } else {
                AdsForMission obj = Instantiate(addMissionButtonPrefab);
                obj.missionUI = this;
                obj.transform.SetParent(missionPlace, false);
            }
        }
    }

    public void Claim(MissionBase m) {
        PlayerData.instance.ClaimMission(m);

        // Rebuild the UI with the new missions
        Open();
    }

    public void Close() {
        var chil = transform.GetChild(0);
        chil.DOScale(0f, 0.1f).OnComplete(() => gameObject.SetActive(false));
    }
}
